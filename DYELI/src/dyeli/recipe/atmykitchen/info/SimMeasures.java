/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dyeli.recipe.atmykitchen.info;


/**
 * Add any similarity measure that you want here.
 * Note: depending on the measure you use, you may need to add or change couple of things in the LanguageModel and ToIdentify classes
 * You can also replace the whole thing with a classifier such as SVM
 * @author Behrang QasemiZadeh <behrangatoffice at gmail.com>
 */
public class SimMeasures {

  
       
    /**
     * get the l2-normed correlation/distance I assume that only features/ngrams
     * presented in the language model counts (so I won't iterate over those
     * features that are not in the languge model)
     *
     * @param record 
     * @param lm the reference model
     * @return
     */
    public static double getL2CorrelateSim(ToIdentify record, LanguageModel lm) {
        double sum = 0.0;
        for (String key : lm.keySet()) {
            // since both vectors length (frequencies) are normalized to 1
            sum += Math.pow(lm.getWeightForFeat(key)-record.getWeightForFeat(key),2);

        }
        return 1 -Math.sqrt(sum); // since length is normalized

    }
    
    
    
    
      /**
     * get cosine similarity
     * @param record
     * @param lm
     * @return 
     */
    public static double cos(ToIdentify record, LanguageModel lm) {
        double sum = 0.0;
        for (String key : record.keySet()) {
            // since both vectors length (frequencies) are normalized to 1
            sum += record.getWeightForFeat(key) * lm.getWeightForFeat(key);

        }
        return sum;

    }
}
