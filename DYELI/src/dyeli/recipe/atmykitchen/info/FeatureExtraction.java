/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dyeli.recipe.atmykitchen.info;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Simple feat extraction, currently only trigrams, ... feel free to add and change. 
 * See also comments in the source code.
 * @author Behrang QasemiZadeh <behrangatoffice at gmail.com>
 */
public class FeatureExtraction {

    final static int setNForNGram = 3; // you can change this

    // this the simplest possible freq count you can use ... 
    // to make sure that there will be no confusion when using the model I factorize the feat extraction method
    /**
     * A simple feature extraction method
     *
     * @param text
     * @return
     */
    public static List<String> simpleFeatExtraction(String text) {
        List<String> extractedList = new ArrayList();
        for (int i = 0; i < text.length() - setNForNGram + 1; i++) {
            String ngram = text.substring(i, i + setNForNGram).trim();
            if (ngram.length() != 0) {
                extractedList.add(ngram);
            }
        }
        return extractedList;
    }

    /**
     * Simple method to update the map that keeps track of frequencies
     *
     * @param map
     * @param ngram
     */
    public static void updateMapFreq(TreeMap<String, Double> map, String ngram) {
        if (map.containsKey(ngram)) {
            Double get = map.get(ngram);
            map.put(ngram, ++get);
        } else {
            map.put(ngram, 1.0);
        }
    }
}
