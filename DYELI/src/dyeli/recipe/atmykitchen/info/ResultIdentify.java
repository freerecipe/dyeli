/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dyeli.recipe.atmykitchen.info;

import java.util.Comparator;

/**
 * The object to store the computed result
 * @author Behrang QasemiZadeh <behrangatoffice at gmail.com>
 */
public class ResultIdentify {
    String langLabel;
    Double weihgt;

    /**
     * 
     * @param langLabel
     * @param weihgt 
     */
    public ResultIdentify(String langLabel, Double weihgt) {
        this.langLabel = langLabel;
        this.weihgt = weihgt;
    }
    
    /**
     * The comparator to sort the results
     */
    public static Comparator<ResultIdentify> simComparator = new Comparator<ResultIdentify>() {

        @Override
        public int compare(ResultIdentify t, ResultIdentify t1) {

            return -1 * Double.compare(t.weihgt, t1.weihgt);
        }

    };

}
