/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dyeli.recipe.atmykitchen.info;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Main class for the training procedure
 *
 * @author Behrang QasemiZadeh <behrangatoffice at gmail.com>
 */
public class GenerateModels {

    /**
     * Main method to generate models \n Please provide:\n + \t(a) path to the
     * directory contains train-text files + \n\t\t Remember: The name of
     * subdirectories are language identifiers + \n\n\t(b)Path to a directory to
     * store model files
     *
     * @param ss
     * @throws IOException
     * @throws Exception
     */
    public static void main(String[] ss) throws IOException, Exception {
        if (ss.length != 2) {
            System.out.println("Please provide:\n"
                    + "\t(a) path to the directory contains train-text files "
                    + "\n\t\t Remember: The name of subdirectories are language identifiers"
                    + "\n\n\t(b)Path to a directory to store model files");
            return;
        }
        String trainMaterialPAth = ss[0];
        File dirLanguages = new File(trainMaterialPAth);
        File[] targeLanguages = dirLanguages.listFiles();

        String pattern = Pattern.quote(System.getProperty("file.separator"));
        for (File langDir : targeLanguages) {
            if (langDir.isDirectory()) {
                String thisLang = langDir.getAbsolutePath().split(pattern)[langDir.getAbsolutePath().split(pattern).length - 1];
                System.out.println("Adding language model for: " + thisLang.toUpperCase());
                File[] dirLang = langDir.listFiles(Utils.filterTxt);
                LanguageModel lm = new LanguageModel(thisLang);
                for (File textFile : dirLang) {
                    lm.addToModel(textFile);
                }
                lm.writeModel(ss[1]);
            }
        }
    }

}
