/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dyeli.recipe.atmykitchen.info;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * Main Class to Manipulate LanguageModel Object
 * Remember this is a dummy implementation!
 * @author Behrang QasemiZadeh <behrangatoffice at gmail.com>
 */
public class LanguageModel extends TreeMap<String, Double> {

    // TreeMap<String, Double> lmModelMap;
    int allCount; // for this dummy impl did not want to iterate over the map twice

    String langIdentifier; // the langaueg identifier label that comes from the name of the train directory

    /**
     * instantiate a new object with the given identifier that comes from the given names to the directories that contain training files
     * @param langIdentifier the language identifier label that comes from the name of the train directory
     */
    public LanguageModel(String langIdentifier) {
        //  lmModelMap = new TreeMap();
        allCount = 0;
        this.langIdentifier = langIdentifier;
    }

    /**
     * Read the file, extract features and add them to the model
     * @param file
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void addToModel(File file) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            List<String> simpleFeatExtraction = FeatureExtraction.simpleFeatExtraction(line);
            for (String ng : simpleFeatExtraction) {
                FeatureExtraction.updateMapFreq(this, ng);
            }
            allCount += simpleFeatExtraction.size();
        }

    }

    /**
     * write the model at the given path using a simple text-based rep
     * @param path
     * @throws FileNotFoundException
     * @throws IOException
     * @throws Exception 
     */
    public void writeModel(String path) throws FileNotFoundException, IOException, Exception {

        if (allCount == 0) {
            throw new Exception("There is no data for " + this.langIdentifier + " \nPlease prove some sample text");
        }
        File pathToW = new File(path + System.getProperty("file.separator"));
        if (!pathToW.exists()) {
            pathToW.mkdirs();
        }

        PrintWriter pw = new PrintWriter(new FileWriter(path + System.getProperty("file.separator") + this.langIdentifier + ".model"));
        //pw.println(langIdentifier);
        for (String ngram : this.keySet()) {
            pw.println(ngram + "\t" + (this.get(ngram) / allCount));
        }
        pw.close();

    }

    /**
     * Static method to load a model that is created previously
     * @param file
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static LanguageModel loadModel(File file) throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        String pattern = Pattern.quote(System.getProperty("file.separator"));

        String thisLang = file.getAbsolutePath().split(pattern)[file.getAbsolutePath().split(pattern).length - 1].split(".model")[0];

        LanguageModel lm = new LanguageModel(thisLang);
        //lm.langIdentifier= line = br.readLine();
        while ((line = br.readLine()) != null) {
            lm.put(line.split("\t")[0], Double.parseDouble(line.split("\t")[1]));

        }
        System.out.println("Loaded language model for " + lm.langIdentifier);
        return lm;

    }

    /**
     * method to access the stored weight for a given feature; if that feature does not exist, then the output is 0.
     * @param feat
     * @return 
     */
    public double getWeightForFeat(String feat) {
        if (this.containsKey(feat)) {
            return this.get(feat);
        } else {
            return 0;
        }

    }

}
