/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dyeli.recipe.atmykitchen.info;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Main class to use the language models in order to identify the language
 *
 * @author Behrang QasemiZadeh <behrangatoffice at gmail.com>
 */
public class UseTheModels {

    /**
     * The main method to predict the language of given files Please provide
     * \n\t(a) path to the directory contains the model files \n\t path to the
     * directory contains files to be checked against the models (file for
     * language identification)\n\t (c) path to the file for dumping the result
     * \n\n** See the source code on choosing the metric for computing
     * similarity
     *
     **
     * @param sss
     * @throws Exception
     */
    public static void main(String[] sss) throws Exception {
        if (sss.length != 3) {
            System.out.println("Please provide ");
            System.out.println("(a) path to the direcotory conatains the model files");
            System.out.println("(b) path to the directory contains files to be checked against the models (file for language identificaiton)");
            System.out.println("(c) path to the file for dumping the result");

        }

        // load the language models to a list, a model per language 
        String loadModel = sss[0];
        File dirLanguages = new File(loadModel);
        List< LanguageModel> listOfLangModel = new ArrayList<>();
        if (!dirLanguages.isDirectory()) {
            throw new Exception("Provide path to director contains language model files !");
        }
        File[] dirLangModel = dirLanguages.listFiles(Utils.modelFile);

        for (File model : dirLangModel) {
            LanguageModel lm
                    = LanguageModel.loadModel(model);
            listOfLangModel.add(lm);
        }

        // examine files one by one and write down the results into a file
        String pathToExam = sss[1];
        File inputDir = new File(pathToExam);
        if (!inputDir.isDirectory()) {
            throw new Exception("Please provide a valid directory name contains text files for examination");
        }
        File[] dirLang = inputDir.listFiles(Utils.filterTxt);

        /// dump the results
        PrintWriter pw = new PrintWriter(sss[2]);
        pw.println("#filename\tidentified-language");
        for (File f : dirLang) {
            List<ResultIdentify> rlList = new ArrayList();
            ToIdentify ti = new ToIdentify(f);
            for (LanguageModel lm : listOfLangModel) {
                double l2 = SimMeasures.cos(ti, lm);
                // I am not sure what is gonna give the best results 
                // for short text cos sim seems to outperform l2 correlate
                // prehaps for short text any measure based on comonolities will outperform the l2correlate --- classic problems on the lenght of doc in doc-classificaiton task
                rlList.add(new ResultIdentify(lm.langIdentifier, l2));
            }
            Collections.sort(rlList, ResultIdentify.simComparator);
            pw.println(f + "\t" + rlList.get(0).langLabel);

        }
        pw.close();

        System.out.println("See the results at: " + sss[2]);
    }

}
