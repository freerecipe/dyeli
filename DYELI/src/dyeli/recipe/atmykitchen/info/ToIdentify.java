/* 
 * Copyright 2015 Behrang QasemiZadeh.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dyeli.recipe.atmykitchen.info;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;

/**
 * To convert input text to a map which can be used later for checking against
 * the language models
 *
 * @author Behrang QasemiZadeh <behrangatoffice at gmail.com>
 */
public class ToIdentify extends TreeMap<String, Double> {

    int count;

    /**
     * Load a file to a treemap (i.e. the vector for the comparison of freqs)
     *
     * @param file the input text file 
     * @throws FileNotFoundException
     * @throws IOException
     */
    public ToIdentify(File file) throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            List<String> simpleFeatExtraction = FeatureExtraction.simpleFeatExtraction(line);
            for (String ng : simpleFeatExtraction) {
                FeatureExtraction.updateMapFreq(this, ng);
            }
            count += simpleFeatExtraction.size();
        }
    }

    /**
     * Get the weight for the feature computed from the given text file
     * @param feat
     * @return 
     */
    public double getWeightForFeat(String feat) {
        if (this.containsKey(feat)) {
            return this.get(feat) / count;
        } else {
            return 0;
        }

    }

}
